import React, {Component} from 'react';

import './search-panel.css';

export default class SearchPanel extends Component {
    state = {
        searchingText: ''
    };
    onSearchChange = (e) => {
        const searchingText = e.target.value;
        this.setState({searchingText});
        this.props.onSearchChange(searchingText);
    };
    render() {
        return (
            <div>
                <input type="text"
                       className="form-control search-input"
                       placeholder="type to search"
                       onChange={this.onSearchChange}
                       value={this.state.searchingText}
                />
            </div>

        );
    }
};

