import React, {Component} from 'react';

import './item-status-filter.css';

export default class ItemStatusFilter extends Component{
    buttons = [
        {name: 'all', label: 'All'},
        {name: 'active', label: 'Active'},
        {name: 'done', label: 'Done'},
    ];
    render() {
        const {onToggleStatus, typeStatus} = this.props;
        return (
                <div className="btn-group">
                    {this.buttons.map(
                        ({name,label}) => {
                            const isActive = typeStatus === name;
                            const clazz = isActive ? 'btn-info' : 'btn-outline-secondary';
                            return (
                                <button id="all" type="button"
                                        className={`btn ${clazz}`} key={name} onClick={() => onToggleStatus(name)}>{label}</button>
                            );
                        }
                    )}
                </div>
            )
    };
}

