import React, {Component} from 'react';

import AppHeader from '../app-header/app-header';
import SearchPanel from '../search-panel/search-panel';
import TodoList from '../todo-list/todo-list';
import ItemAddForm from '../item-add-form';
import ItemStatusFilter from '../item-status-filter/item-status-filter';
import './app.css';

export default class App extends Component  {

    maxId = 100;

    state = {
        todoData: [
            this.createTodoItem('Overeat ice cream'),
            this.createTodoItem('Walk with Syama'),
            this.createTodoItem('Do something')
        ],
        searchingText: '',
        typeStatus: 'all'
    };

    createTodoItem(label) {
        return {
            label,
            important: false,
            done: false,
            id: this.maxId++
        }
    }

    deleteItem = (id) => {
        this.setState(({todoData}) => {
            const idx = todoData.findIndex((el) => el.id === id);
            const before = todoData.slice(0, idx);
            const after = todoData.slice(idx+1);
            const newArray = [...before, ...after];

            return {
                todoData: newArray
            }
        });
    };

    addItem = (text) => {
        // generate id
        const newItem = this.createTodoItem(text);
        // add element in array
        this.setState(({todoData}) => {
            const newArray = [...todoData, newItem];
            return {
                todoData: newArray
            }
        });
    };

    toggleProperty(arr, id, propName) {
        const idx = arr.findIndex((el) => el.id === id);
        //1. update obj
        const oldItem = arr[idx];
        const newItem = {...oldItem, [propName]: !oldItem[propName]};

        // 2. construct new array
        const before = arr.slice(0, idx);
        const after = arr.slice(idx+1);
        const newArray = [...before,newItem, ...after];

        return newArray;
    }
    onToggleIportant = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'important')
            };
        });
    };

    onToggleDone = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'done')
            };
        });
    };
    onSearchChange = (searchingText) => {
      this.setState(()=> {
          return {
              searchingText
          }
      });
    };
    searchItems = (items, searchingText) => {
        if (searchingText === '') {
            return items
        }
        return items.filter((el) => {
            return el.label
                .toLowerCase()
                .includes(searchingText.toLowerCase())
        });
    };

    // change status
    onToggleStatus = (typeStatus) => {
        this.setState(()=> {
            return {
                typeStatus
            }
        });
    };
    filterItems = (items, typeStatus) => {
        switch (typeStatus) {
            case 'all':
                return items;
            case 'active':
                return items.filter((el) => {
                return !el.done
            });
            case 'done':
                return items.filter((el) => {
                return el.done
            })
        }
    };

    render() {

        const {todoData, searchingText,typeStatus} = this.state;
        const doneCount = todoData
                            .filter((el)=> el.done ).length;
        const todoCount = todoData.length - doneCount;
        // console.log('typeStatus');
        // console.log(typeStatus);
        const visibleItems = this.searchItems(todoData, searchingText);
        const filteredItems = this.filterItems(visibleItems, typeStatus);
        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount}/>
                <div className="top-panel d-flex">
                    <SearchPanel items={todoData} onSearchChange={this.onSearchChange}/>
                    <ItemStatusFilter typeStatus={typeStatus} onToggleStatus={this.onToggleStatus} />
                </div>

                <TodoList
                todos={filteredItems}
                onDeleted={this.deleteItem}
                onToggleImportant={this.onToggleIportant}
                onToggleDone={this.onToggleDone}
                />
                <ItemAddForm onItemAdded={this.addItem}/>
            </div>
        );
    }

};
