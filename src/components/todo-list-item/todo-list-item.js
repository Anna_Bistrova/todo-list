import React, {Component} from 'react';

import './todo-list-item.css';

export default class TodoListItem extends Component {

    constructor() {
        super();

        // this.onLabelClick = () => {
        //     this.setState(({done, count}) => {
        //         return {
        //             done: !done,
        //             count: count+1
        //         }
        //
        //     })
        // };
        // this.onMarkImportant = () => {
        //     this.setState((state) =>{
        //         return {
        //             important: !state.important
        //         }
        //
        //     })
        // };

        // this.state = {
        //     done: false,
        //     important: false,
        //     count: 0
        // }
    }
    render() {
        // console.log('label');
        // console.log(this.props.label);
        // console.log('count');
        // console.log(this.state.count);
        const {label, onDeleted, onToggleImportant, onToggleDone, important, done } = this.props;
        // const {done, important} = this.state;


        let classNames = 'todo-list-item';
        if (done) {
            classNames += ' done';
        }

        if (important) {
            classNames += ' important'
        }


        return (
            <span className={classNames}>
              <span
                  className="todo-list-item-label"
                  onClick={onToggleDone}
              >
                {label}
              </span>

              <button type="button"
                      className="btn btn-outline-success btn-sm float-end"
                      onClick={onToggleImportant}
              >
                <i className="fa fa-exclamation" />

              </button>

              <button type="button"
                      className="btn btn-outline-danger btn-sm float-end"
                      onClick={onDeleted}>
                <i className="fa fa-trash-o" />
              </button>
            </span>
        );
    }
}

